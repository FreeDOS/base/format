/*
// Program:  Format
*/

#ifndef MSG_HELPER_H
#define MSG_HELPER_H

#include "kitten.h"

void Key_For_Next_Page(void);
void Print_Messages_With_Pauses(nl_catd catalog, int setnum, char const * const messages[]);

#endif
