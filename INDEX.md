# Format

Disk formatting program that creates FAT file systems and low-level formats floppy disks

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## FORMAT.LSM

<table>
<tr><td>title</td><td>Format</td></tr>
<tr><td>version</td><td>0.92a (pre-release)</td></tr>
<tr><td>entered&nbsp;date</td><td>2024-08-07</td></tr>
<tr><td>description</td><td>Disk formatting program</td></tr>
<tr><td>summary</td><td>Disk formatting program that creates FAT file systems and low-level formats floppy disks</td></tr>
<tr><td>keywords</td><td>freedos, format, fat12, fat16, fat32, floppy, harddisk, mirror, unformat</td></tr>
<tr><td>author</td><td>Brian E. Reifsnyder &lt;reifsnyderb (#) mindspring.com&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/format</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU GPL](LICENSE)</td></tr>
</table>
